package model;

public class Meter {
    Integer meterID;
    String contractID;
    String month;
    Integer year;
    Integer kwCounter;

    @Override
    public String toString() {
        return "Meter{" +
                "meterID=" + meterID +
                ", contractID='" + contractID + '\'' +
                ", month='" + month + '\'' +
                ", year=" + year +
                ", kwCounter=" + kwCounter +
                '}';
    }

    public Integer getMeterID() {
        return meterID;
    }

    public void setMeterID(Integer meterID) {
        this.meterID = meterID;
    }

    public String getContractID() {
        return contractID;
    }

    public void setContractID(String contractID) {
        this.contractID = contractID;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getKwCounter() {
        return kwCounter;
    }

    public void setKwCounter(Integer kwCounter) {
        this.kwCounter = kwCounter;
    }

    public Meter(Integer meterID, String contractID,
                 String month, Integer year, Integer kwCounter) {
        this.meterID = meterID;
        this.contractID = contractID;
        this.month = month;
        this.year = year;
        this.kwCounter = kwCounter;
    }
}
