package model;
public class UserRegistration {
    private String firstName;
    private String lastName;
    private String birthday;
    private String emailID;
    private String phoneNr;
    private String address;
    private String contractID;
    private String username;
    private String password;

    @Override
    public String toString() {
        return "UserSignUp{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday='" + birthday + '\'' +
                ", emailID='" + emailID + '\'' +
                ", phoneNr='" + phoneNr + '\'' +
                ", address='" + address + '\'' +
                ", contractID='" + contractID + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContractID() {
        return contractID;
    }

    public void setContractID(String contractID) {
        this.contractID = contractID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRegistration(String firstName, String lastName,
                      String birthday, String emailID, String phoneNr,
                      String address, String contractID,
                      String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.emailID = emailID;
        this.phoneNr = phoneNr;
        this.address = address;
        this.contractID = contractID;
        this.username = username;
        this.password = password;
    }
}


