package model;

public class UserSignIn {
    private String name;
    private String username;
    private String password;

    public UserSignIn(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = password;
    }
}
